from os import write
from discord.ext import tasks
import discord
import twitter
import io
import datetime
import json
from generate_unconditional_samples import sample_model
from interactive_conditional_samples import interact_model

client = discord.Client()

#all time units are in minutes
maintenance_time = 10
post_time = 180
post_track_time = 1440 #24 hours

tracked_wisdom = {}
tweet_emojis = []

with open('secret.json', 'r') as s:
    secret = json.load(s)
    token = secret["token"]
    channel_name = secret["channel_name"]
    waiting_message = secret["waiting_message"]
    twitter_bot_name = secret["twitter_bot_name"]
    t_api_key = secret["t_api_key"] 
    t_api_secret = secret["t_api_secret"]
    t_access_key = secret["t_access_key"]
    t_access_secret = secret["t_access_secret"]
    t_channel_name1 = secret["t_channel_name1"] 
    t_channel_name2 = secret["t_channel_name2"]
    blacklist = secret["blacklist"]

tapi = twitter.Api(consumer_key=t_api_key, 
                    consumer_secret=t_api_secret,
                    access_token_key=t_access_key,
                    access_token_secret=t_access_secret)

def get_time():
    now = datetime.datetime.now()
    return now.strftime("%H:%M:%S") + " | "

def log(msg):
    msg = str(msg)
    print(get_time() + msg)
    with open('log.txt', 'a') as logfile:
        logfile.write(get_time() + msg + '\n')

#loads emojis declared in emojis.json for use in reactions on wisdom
async def get_emojis():
    await client.wait_until_ready()
    loaded_emojis = client.emojis
    with open('emojis.json', 'r') as c:
        content = json.load(c)
        for e in loaded_emojis:
            if str(e.id) in content.values():
                tweet_emojis.append(e)

#returns generated text from model
async def get_wisdom(): 
    return sample_model()

async def get_wig(wig):
    return interact_model(prompt=wig)

#scrapes text from all possible text channels
async def scrape_all_trash(guild):
    await client.wait_until_ready()
    channel = None
    global blacklist
    log("Not scraping channels: " + ', '.join(blacklist))
    for c in client.get_all_channels():
            if(c.name not in blacklist and c.guild == guild and c.type != discord.ChannelType.category and c.type != discord.ChannelType.voice):
                channel = c
                with io.open('data.txt', 'a', encoding="utf-8") as f:
                    try:
                        log("Starting scrape for channel: {0}".format(channel.name))
                        async for message in channel.history(limit=None):
                            if not message.attachments and not message.embeds:
                                if message.author != client.user:
                                    if not message.content.startswith(" ") and not message.content.startswith('\n') and not message.content.startswith('!'):
                                        f.write(message.content+'\n')
                        log("Finished scraping channel: {0}".format(channel.name))
                    except Exception as err:
                        f.close()
                        log("Scrape failed in channel {0}: {1} ".format(channel.name, err))
    log("Finished scraping")

#scrape text from channel pointed at by channel_name
async def scrape_trash(guild):
    await client.wait_until_ready()
    channel = None
    for c in client.get_all_channels():
        if(c.name == channel_name):
            channel = c
            with io.open('data.txt', 'w', encoding="utf-8") as f:
                try:
                    async for message in channel.history(limit=None,after=datetime.datetime(year=2020, month=1, day=20)):
                        if not message.attachments:
                            if message.author != client.user:
                                if not message.content.startswith(" ") and not message.content.startswith('\n') and not message.content.startswith('!'):
                                    f.write(message.content+'\n')
                except Exception as err:
                    f.close()
                    log("Scrape failed in channel {0}: {1} ".format(channel.name, err))

#runs depending on set time interval
#posts wisdom to channel specified in t_channel_name
@tasks.loop(minutes=post_time)
async def tweet_wisdom():
    global tracked_wisdom
    global tweet_emojis
    channel = None
    for c in client.get_all_channels():
        if(c.name == t_channel_name1 or c.name == t_channel_name2):
            channel = c
            log("Posting to " + channel.name)
            wisdom = await channel.send(waiting_message)
            tracked_wisdom[wisdom.id] = post_track_time
            await wisdom.edit(content=await get_wisdom())
            for e in tweet_emojis:
                await wisdom.add_reaction(e)

@tweet_wisdom.before_loop
async def before():
    log("Waiting to post...")
    await client.wait_until_ready()

@client.event
async def on_reaction_add(reaction, user):
    global tracked_wisdom
    global tweet_emojis
    wisdom = reaction.message
    if wisdom.author == client.user:
        if wisdom.id in tracked_wisdom.keys():
            count = 0
            for e in wisdom.reactions:
                if e.emoji in tweet_emojis:
                    count = count + e.count
            if count > 4:
                try:
                    log("Posting to twitter")
                    status = tapi.PostUpdate(wisdom.content[:280])
                    await wisdom.edit(content="https://twitter.com/" + twitter_bot_name + "/status/"+ status.id_str)
                    del tracked_wisdom[wisdom.id]
                except Exception as err:
                    log("Error occured trying to post message {0}: {1}".format(wisdom.id, err))

@tasks.loop(minutes=maintenance_time)
async def maintenance():
    await client.wait_until_ready()
    global tracked_wisdom
    log("Currently tracked posts:")
    log(tracked_wisdom)
    try:
        deleteTweets = []
        for m_id, m_t in tracked_wisdom.items():
            tracked_wisdom[m_id] = m_t - maintenance_time
            if m_t - maintenance_time == 0:
                deleteTweets.append(m_id)
        for m_id in deleteTweets:
            del tracked_wisdom[m_id]
    except Exception as err:
        log("Maintenance failed: {0}".format(err))

@client.event
async def on_ready():
    log('We have logged in as {0.user}'.format(client))
    await client.wait_until_ready()
    await get_emojis()

@client.event
async def on_message(message):
    global tracked_wisdom
    if message.content.startswith('!wisdom') or message.content.startswith('!widsom'):
        wisdom = await message.channel.send(waiting_message)
        tracked_wisdom[wisdom.id] = 240
        await wisdom.edit(content=await get_wisdom())
        for e in tweet_emojis:
            await wisdom.add_reaction(e)
    elif message.content.startswith('!wig'):
        content = message.content.split(' ', 1)
        if len(content) > 1:
            wisdom = await message.channel.send(waiting_message)
            tracked_wisdom[wisdom.id] = 240
            wig = content[1]
            wig = wig + await get_wig(wig)
            await wisdom.edit(content=wig)
            for e in tweet_emojis:
                await wisdom.add_reaction(e)
    elif message.content.startswith('!collectwisdom'):
        await scrape_trash(message.guild)
    elif message.content.startswith('!collectallwisdom'):
        await scrape_all_trash(message.guild)

maintenance.start()
tweet_wisdom.start()

client.run(token)
