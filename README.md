# mol-bot

# Requirements
Python 3.X
Tensorflow 2.X

**IF YOU WANT TO USE YOUR GPU** 

CUDA/cuDNN, version required is the one that the Tensorflow library you are using for the training is compiled against

If using 3000 series RTX cards, install tf-nightly and tf-nightly-gpu instead of tensorflow/gpu and use CUDA 11.1 (CUDA 10.2 might be required because of a DLL requirement in the current nightly TF build)

# Dependencies
Install using 
`$ python -m pip install -r requirements.txt`

might need to install numpy separately idk

# GPT2 Use
To train model
1.  Run 

    `$ python download_model.py 117M`

2.  Get a large set of text data and put it into a file named `data.txt` and drop it into `src/` 
3.  Run 

    `$ python encode.py data.txt data.npz`
4.  If you want more options look into `train.py` but otherwise you can just go ahead and do:

    `$ python train.py --dataset data.npz`
5. Stop training with Ctrl+c and it will save a checkpoint, training can be stopped and resumed this way at any time

To use model
1. In `src/models/` create a folder called `mol`, this is the default but it can be changed in `generate_unconditional_samples.py`
2. Go to `src/checkpoint/run1` and copy the files `checkpoint`,`model-xxx.data-0000-of-00001`,`model-xxx.index`, `model-xxx.meta` into your `mol` folder
3. Go to `src/models/117M` and copy `encoder.json`,`hparams.json`,`vocab.bpe` into your `mol` folder

# Discord Bot Use
1. Rename `secret.json.template` to `secret.json` and fill in all necessary fields
2. Run with `$ python main.py`
3. Type `!wisdom` in a channel to get the bot to generate some text
4. Type `!wig {prompt}` in a channel to get the bot to generate some text with a given prompt
5. Type `!collectwisdom` to scrape the channel specified in `secret.json` for text and put it into a `data.txt` file
6. Type `!collectallwisdom` to scrape all channels not specified in the blacklist contained in `secret.json`